# Another way to push your message on twitter
import tweepy

def get_api(cfg):
  auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
  auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
  return tweepy.API(auth)

def main():
  # Fill in the values noted in previous step here
  cfg = { 
    "consumer_key"        : " Enter Your key here",
    "consumer_secret"     : " Enter Your key here",
    "access_token"        : " Enter Your key here",
    "access_token_secret" : "Enter Your key here "
    }

  api = get_api(cfg)
  tweet = "It's me priyank"
  status = api.update_status(status=tweet) 
  # Yes, tweet is called 'status' rather confusing

if __name__ == "__main__":
  main()
